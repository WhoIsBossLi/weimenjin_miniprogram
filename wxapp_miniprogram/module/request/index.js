let member = require('../member/index');

let HttpPost = async function (url, data = {}) {
    let token = wx.getStorageSync('token')
    if (!token){
        await member.Login().catch((err) => {
            console.log("loginerr", err)
        })
    }
    wx.showLoading({
        title: '加载中',
    });
   return  new Promise(   function (resolve, reject) {

       data.httpPost =1
        wx.request({
            url: "https://wxapp.wmj.com.cn/api/" +url,
            method: "POST",
            header: {
                "Authorization": wx.getStorageSync('token')
            },
            data: data,
            success: async function (res) {
              wx.stopPullDownRefresh();
              wx.hideLoading();
                console.log("res",res)
               let resData = res.data
                if (resData.code ===0 || resData.status==="200"|| resData.opendoor_status==="200") {

                    resolve(res.data);
                    return
                } else {
                    if (resData.code===101){

                        await member.Login().catch((err) => {
                            console.log("loginerr", err)
                        })

                       let reRequest = await HttpPost(url,data).catch((err)=>{
                           reject(err);
                           return
                       })

                        resolve(reRequest);
                        return
                    }
                  wx.showToast({
                    title: resData.msg,
                    icon: 'none',
                    mask: true, // 防止触摸穿透
                    duration: 2000
                });

                    reject(resData);
                    return
                }

            },
            fail: function (res) {
              wx.stopPullDownRefresh();
              wx.hideLoading();
              wx.showToast({
                title: '网络故障，请稍后重试',
                icon: 'none',
                mask: true, // 防止触摸穿透
                duration: 2000
            });
                reject(res);
            }
        });

    })
}


module.exports = {
    HttpPost
}
