<?php


namespace app\module\lockAuthServer;


class LockAuth
{
    static function Add($lock_id,$member_id,$user_id){

        $authdata['lock_id']=$lock_id;
        $authdata['member_id']=$member_id;
        $authdata['auth_member_id']=0;
        $authdata['auth_shareability']=1;
        $authdata['auth_sharelimit']=0;
        $authdata['auth_openlimit']=0;
        $authdata['auth_starttime']=time();
        $authdata['auth_isadmin']=1;
        $authdata['auth_status']=1;
        $authdata['user_id']=$user_id;
        \xhadmin\service\api\LockAuthService::applyauth($authdata);
    }
}
