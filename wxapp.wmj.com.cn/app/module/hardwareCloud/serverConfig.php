<?php


namespace app\module\hardwareCloud;

class serverConfig
{
    static $WiFIUrl = "https://wdev.wmj.com.cn/deviceApi/";
    static $AppId = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
    static $AppSecret = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

    static function GetUrl(){
        return self::$WiFIUrl;
    }

    static function GetAppId(){
        return self::$AppId;
    }

    static function GetAppSecret(){
        return self::$AppSecret;
    }

}
