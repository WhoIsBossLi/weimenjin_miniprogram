<?php


namespace app\api\controller\device;


use app\module\code\Code;
use app\module\hardwareCloud\HardwareClout;
use think\facade\Db;

class Finger
{
    public function list()
    {

        $lock_id = input("lock_id");
        $fingerS = Db::name("finger")->where(["lock_id" => $lock_id])->whereNull("deleted_at")->select()->toArray();
        return json(Code::CodeOk([
            "data" => $fingerS
        ]));
    }
    public function info()
    {

        $finger_id = input("finger_id");
        $fingerS = Db::name("finger")->where(["finger_id" => $finger_id])->find();
        return json(Code::CodeOk([
            "data" => $fingerS
        ]));
    }
    public function add()
    {
        $lock_id = input("lock_id");

        $finger_name = input("finger_name");
        $end_time = strtotime(input("end_time"));
        $lock = Db::name("lock")->where(["lock_id" => $lock_id])->find();


       $addres= HardwareClout::WifiLock()->FingerAdd($lock["lock_sn"],  $lock["device_cid"], time(), $end_time);
        if($addres["err"]){
            return json(Code::CodeErr(1000,$addres["err"]));
        }
        Db::name("finger")->insert([
            "lock_id" => $lock_id,
            "fp_id" => $addres["info"]["fp_id"],
            "finger_name" => $finger_name,
            "end_time" => $end_time,
            "created_at" => time(),
        ]);
        return json(Code::CodeOk([]));
    }

    public function edit()
    {
        $lock_id = input("lock_id");
        $finger_id = input("finger_id");

        $finger_name = input("finger_name");
        $end_time = strtotime(input("end_time"));

        $lock = Db::name("lock")->where(["lock_id" => $lock_id])->find();
        $finger= Db::name("finger")->where(["finger_id" => $finger_id])->find();


        $addres= HardwareClout::WifiLock()->FingerEdit($lock["lock_sn"],  $lock["device_cid"],$finger["fp_id"], time(), $end_time);
        if($addres["err"]){
            return json(Code::CodeErr(1000,$addres["err"]));
        }
        Db::name("finger")->where(["finger_id"=>$finger_id])->update([

            "finger_name" => $finger_name,
            "end_time" => $end_time,

        ]);
        return json(Code::CodeOk([]));
    }
    public function del()
    {
        $lock_id = input("lock_id");
        $finger_id = input("finger_id");
        $lock = Db::name("lock")->where(["lock_id" => $lock_id])->find();
        $finger = Db::name("finger")->where(["finger_id" => $finger_id])->find();
        $addres= HardwareClout::WifiLock()->FingerDel($lock["lock_sn"], $finger["fp_id"], $lock["device_cid"]);
        if($addres["err"]){
            return json(Code::CodeErr(1000,$addres["err"]));
        }

        Db::name("finger")->where(["finger_id" => $finger_id])->update(["deleted_at"=>date("Y-m-d H:i:s")]);
        return json(Code::CodeOk([]));
    }
}
