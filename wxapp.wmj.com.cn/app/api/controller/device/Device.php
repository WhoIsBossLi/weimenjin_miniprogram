<?php


namespace app\api\controller\device;


use app\api\controller\Base;
use app\BaseController;
use app\module\code\Code;
use app\module\lockServer\LockLog;
use app\module\hardwareCloud\HardwareClout;
use think\facade\Db;
use xhadmin\db\Lock as LockDb;

class Device extends Base
{
    function electricityStart(){
        $lock_id=input("lock_id");
        $lockInfo= LockDb::getInfo($lock_id);
        $OpenLock = HardwareClout::AirSwitch()->ElectricityStart($lockInfo["lock_sn"]);
        $member_id =$this->_data["uid"];
        if($OpenLock["err"]){
            LockLog::add($member_id,$lock_id,5,0);
            return  json(Code::CodeErr(1000,($OpenLock["err"])));
        }
        LockLog::add($member_id,$lock_id,5,1);
        return  json(Code::CodeOk(["msg"=>"开电成功"]));

    }

    function electricityStop(){
        $lock_id=input("lock_id");
        $lockInfo= LockDb::getInfo($lock_id);
        $OpenLock = HardwareClout::AirSwitch()->ElectricityStop($lockInfo["lock_sn"]);
        $member_id =$this->_data["uid"];
        if($OpenLock["err"]){
                 LockLog::add($member_id,$lock_id,6,0);
            return  json(Code::CodeErr(1000,($OpenLock["err"])));
        }
         LockLog::add($member_id,$lock_id,6,1);
        return  json(Code::CodeOk(["msg"=>"关电成功"]));

    }

    function info(){
        $lock_id=input("lock_id");
        $lockInfo= LockDb::getInfo($lock_id);
        $OpenLock = HardwareClout::AirSwitch()->Getdevinfo($lockInfo["lock_sn"]);
        if($OpenLock["err"]){
            return  json(Code::CodeErr(1000,($OpenLock["err"])));
        }
        if($OpenLock["data"]["info"]["switch_state"]==1){
            $OpenLock["data"]["info"]["switch_state"]="接通";
        }else{
            $OpenLock["data"]["info"]["switch_state"]="断开";
        }

        Db::name("lock")->where(["lock_id"=>$lock_id])->update([
            "imei"=>$OpenLock["data"]["info"]["imei"],
            "iccid"=>$OpenLock["data"]["info"]["iccid"],
        ]);


        return  json(Code::CodeOk(["msg"=>"获取成功","data"=>$OpenLock["data"]["info"],"sql"=> Db::name("lock")->getLastSql()]));

    }

}
